//
//  ViewController.m
//  liveperson
//
//  Created by Richard Morgan on 03/11/2016.
//  Copyright © 2016 Richard Morgan. All rights reserved.
//

#import "ViewController.h"
#import <AssistSDK.h>
#import <ACBClientSDK/ACBUC.h>
#import <UIKit/UIKit.h>

@interface ViewController  ()


@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

static float const FromLeftHandSide = 70.0f;
static float const FromBottom = 140.0f;
static float const EndSupportTextWidth = 150.0f;
static float const EndSupportTextHeight = 60.0f;

@implementation ViewController

UIButton *endSupportButton;

static ACBUC *acbuc;

static ACBClientCall *curentCall;

+ (ACBUC *) getACBUC
{
    return acbuc;
}


- (void)registerDefaultsFromSettingsBundle {
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if(!settingsBundle) {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    //[defaultsToRegister release];
}


- (void) setEndShortCodeSupportButtonPosition {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    endSupportButton.frame = CGRectMake(FromLeftHandSide, screenSize.height - FromBottom, EndSupportTextWidth, EndSupportTextHeight);
}

- (void) viewDidLayoutSubviews {
    [self setEndShortCodeSupportButtonPosition];
}

- (void) addEndShortCodeSupportButton {
    
    ///image here
    
    //NSString *buttonEndImageUrl = [SupportParameters buttonEndImageUrl];
    
    //NSURL *url = [NSURL URLWithString:buttonEndImageUrl];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    //UIImage *endImage = [[UIImage alloc] initWithData:data];
    
    //endSupportButton = [UIButton buttonWithType: UIButtonTypeCustom];
    //[endSupportButton setImage: endImage forState: UIControlStateNormal];
    //[endSupportButton setImage: endImage forState: UIControlStateHighlighted];
    
    [endSupportButton setFrame: CGRectMake(70,140 , 150, 60)];
    
    endSupportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [endSupportButton.layer setBorderColor:[[UIColor redColor] CGColor]];
    [endSupportButton.layer setBorderWidth:2.0f];
    
    [endSupportButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [endSupportButton setBackgroundColor:[UIColor redColor]];
    
    [endSupportButton addTarget:self action:@selector(endShortCodeSupport:) forControlEvents:UIControlEventTouchUpInside];
    [endSupportButton setTitle:@"End Co-browse" forState:UIControlStateNormal];
    
    [self setEndShortCodeSupportButtonPosition];
    [self hideEndSupportButton];
    
    [self.view addSubview:endSupportButton];
    NSLog(@"xxx : addEndShortCodeSupportButton");
    
    
}

- (void) showEndSupportButton {
    endSupportButton.hidden = NO;
}

- (void) hideEndSupportButton {
    endSupportButton.hidden = YES;
}

- (IBAction) endShortCodeSupport:(id) sender {
    NSLog(@"Ending short code support");
    [self hideEndSupportButton];
    [AssistSDK endSupport];
}

-(void) onEndSupport {
    
    [self hideEndSupportButton];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.webView.delegate = self;
    
    self.remoteView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    self.remoteView.opaque = NO;
    self.remote.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    self.remote.opaque = NO;
    
    [self.remoteView setHidden:YES];

    
    // MAKE DRAGGABLE
    self.remoteView.userInteractionEnabled = YES;
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(remoteDragged:)];
    [self.remoteView addGestureRecognizer:gesture];
    
    [self registerDefaultsFromSettingsBundle];
    [self addEndShortCodeSupportButton];
    
    NSString *viewUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"view_url"];
    
    NSLog(@"xxx This is it: %@", viewUrl);
    
    //NSString *urlAddress = @"https://cafexsup.bitbucket.io/?accountId=72536072&this=mobile-demo&that=mobile-training";
    //NSURL *url = [NSURL URLWithString:urlAddress];
    
    
    NSURL *url = [NSURL URLWithString:viewUrl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
            // FCSDK CALL SETUP
        
        
            NSString *videoServer = [[NSUserDefaults standardUserDefaults] stringForKey:@"video_server"];
        
            NSLog(@"::videoServer: %@", videoServer);
        
            NSString *urlString = [NSString stringWithFormat: @"%@", videoServer];

        
            // NSString *urlString = [NSString stringWithFormat:@"https://79243728.ngrok.io/session.php?username=demo&password=6edc02d4"];
        
            // NSString *urlString = [NSString stringWithFormat:@"https://www.cafex.com/microsoft-video/session.php"];
        
        
            NSLog(@"::urlString: %@", urlString);

        
            NSURL *sessionUrl = [NSURL URLWithString:urlString];
            
            NSLog(@"::Session: %@", sessionUrl);
            
            NSString *sessionID = [NSString stringWithContentsOfURL:sessionUrl encoding:NSASCIIStringEncoding
                                                              error:nil];
            NSString *trimmed = [sessionID stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            NSLog(@"::Session: %@", sessionID);
            
            NSLog(@"::Trimmed: %@", trimmed);
            
            // now init the with the session id
            acbuc = [ACBUC ucWithConfiguration:sessionID delegate:self];
            
            [acbuc setNetworkReachable:YES];
            [acbuc  acceptAnyCertificate:YES];
            
            [acbuc startSession];
            
            NSLog(@"::Session: %@", sessionID);
        
        });
   

}


- (void)remoteDragged:(UIPanGestureRecognizer *)gesture
{
    UILabel *view = (UILabel *)gesture.view;
    CGPoint translation = [gesture translationInView:view];
    
    // move label
    view.center = CGPointMake(view.center.x + translation.x,
                                view.center.y + translation.y);
    
    // reset translation
    [gesture setTranslation:CGPointZero inView:view];
}


-(void)ucDidStartSession:(ACBUC *)uc
{

    NSLog(@"::ucDidStartSession");
    
    // assign acbuc object and init local video stream
    self.acbuc = [ViewController getACBUC];
    
    //self.acbuc.phone.previewView = self.local;
    
    ACBClientPhone* phone = self.acbuc.phone;
    phone.delegate = self;
   // phone.previewView = self.local;
    
    [ACBClientPhone requestMicrophoneAndCameraPermission:true video:true];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        /* Your UI code */
    
        
    
    });
    
}

-(void)ucDidFailToStartSession:(ACBUC *)uc
{
    NSLog(@"::ucDidFailToStartSession");
    
    NSLog(@" uc %@", uc);
    
}


- (void) phone:(ACBClientPhone*)phone didReceiveCall:(ACBClientCall*)call {
    
    [NSThread sleepForTimeInterval:1.0];
    
    
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [self.remoteView setHidden:NO];
            
       
            curentCall = call;
        
            NSLog(@"::didReceiveCall %@", call);
        
            ACBClientCall *newCall = call;
        
            newCall.delegate = self;
        
            newCall.videoView = self.remote;
        
            [newCall answerWithAudio:YES video:YES];
        
        });

}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //Check here if still webview is loding the content
    if (webView.isLoading)
        return;
    
    //after code when webview finishes
    NSLog(@"Webview loading finished");
    
    //NSString *accountId = @"72536072";
    
    NSString *accountId = [[NSUserDefaults standardUserDefaults] stringForKey:@"account_id"];
    
    
    NSLog(@"accountId : %@", accountId);
    
    NSString *accountJsString = [NSString stringWithFormat: @"var accountId = '%@';", accountId];
    
     //NSString *accountJsString = [NSString stringWithFormat: @"alert('%@');", accountId];
    
     NSLog(@"accountJsString : %@", accountJsString);
    
    
    [self.webView stringByEvaluatingJavaScriptFromString:accountJsString];
    
    NSString *viewUrlString = [[NSUserDefaults standardUserDefaults] stringForKey:@"js_url"];
    
    NSURL *jsUrl = [NSURL URLWithString:viewUrlString];
    NSString *stringFromFileAtURL = [[NSString alloc]
                                     initWithContentsOfURL:jsUrl
                                     encoding:NSUTF8StringEncoding
                                     error:NULL];
        
    //NSString *js = [NSString stringWithContentsOfFile:stringFromFileAtURL encoding:NSUTF8StringEncoding error:NULL];
    
    NSString *cssString = @".assist-cobrowsing .default-cobrowsing {background-color:#FFFC9E;font-size:12px;color:#444444;height:25px;line-height:25px;border:1px solid #CCC;opactiy:1;}";
    NSString *javascriptString = @"var style = document.createElement('style'); style.innerHTML = '%@'; document.head.appendChild(style)";
    NSString *javascriptWithCSSString = [NSString stringWithFormat:javascriptString, cssString];
    [webView stringByEvaluatingJavaScriptFromString:javascriptWithCSSString];
    
    [self.webView stringByEvaluatingJavaScriptFromString:stringFromFileAtURL];
    
    NSLog(@"stringFromFileAtURL : %@", stringFromFileAtURL);
    
    
}



- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([[request.URL scheme] isEqual:@"yourapp"]) {
        
            NSLog(@"HERE!!!! %@", request.URL);
            
            NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:request.URL resolvingAgainstBaseURL:NO];
            NSArray *queryItems = urlComponents.queryItems;
        
            NSString *token = [self valueForKey:@"token" fromQueryItems:queryItems];
            NSLog(@"TOKEN: %@", token);
        
            NSString *server = [self valueForKey:@"url" fromQueryItems:queryItems];
            NSLog(@"SERVER: %@", server);
        
            NSString *cid = [self valueForKey:@"cid" fromQueryItems:queryItems];
            NSLog(@"CID: %@", cid);
        
            //Start Support here
        
            NSDictionary *laConfig = [NSDictionary dictionaryWithObjectsAndKeys: cid, @"correlationId",  token, @"sessionToken", self, @"screenShareRequestedDelegate", nil];
            [AssistSDK startSupport: server
                  supportParameters:laConfig];
            [self showEndSupportButton];
        

        return NO; // Tells the webView not to load the URL
    } else {
        return YES;
    }
}




- (NSString *)valueForKey:(NSString *)key
           fromQueryItems:(NSArray *)queryItems
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems
                                  filteredArrayUsingPredicate:predicate]
                                 firstObject];
    return queryItem.value;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) assistSDKScreenShareRequested:(void (^)(void))allow deny:(void (^)(void))deny {
    
    NSLog(@"Request for screen share called.");
    
    allow();
    
    // OR deny() if want to automatically reject all screen share requests.
}

-(void)logMessage:(NSString*)message
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        //        self.log.text = [NSString stringWithFormat:@"%@\n%@", message, self.log.text];
        NSLog(@":::%@", message);
        
    });
}




- (void) call:(ACBClientCall*)call didChangeStatus:(ACBClientCallStatus)status
{
    switch (status)
    {
        case ACBClientCallStatusRinging:
            break;
        case ACBClientCallStatusAlerting:
            break;
        case ACBClientCallStatusMediaPending:
            break;
        case ACBClientCallStatusInCall:
            break;
        case ACBClientCallStatusEnded:
            [self updateUIForEndedCall:call];
            break;
        case ACBClientCallStatusSetup:
            break;
        case ACBClientCallStatusBusy:
        case ACBClientCallStatusError:
        case ACBClientCallStatusNotFound:
        case ACBClientCallStatusTimedOut:
            [self updateUIForEndedCall:call];
            break;
    }
}

- (void) updateUIForEndedCall:(ACBClientCall *)call
{
     [self.remoteView setHidden:YES];
    
}




- (IBAction)endCall:(id)sender {
    
    [curentCall end];
    [self.remoteView setHidden:YES];
    
}


@end
