//
//  main.m
//  liveperson
//
//  Created by Richard Morgan on 03/11/2016.
//  Copyright © 2016 Richard Morgan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
