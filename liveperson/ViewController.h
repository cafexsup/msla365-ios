//
//  ViewController.h
//  liveperson
//
//  Created by Richard Morgan on 03/11/2016.
//  Copyright © 2016 Richard Morgan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssistSDK.h"
#import "AppDelegate.h"
#import <ACBClientSDK/ACBUC.h>


@interface ViewController : UIViewController <UIWebViewDelegate,ASDKScreenShareRequestedDelegate, AssistSDKDelegate,ACBClientCallDelegate, ACBUCDelegate, ACBClientPhoneDelegate>

@property (weak, nonatomic) IBOutlet UIView *remote;
@property (weak, nonatomic) IBOutlet UIView *local;
@property (strong, nonatomic) IBOutlet UIView *remoteView;


@property (weak, nonatomic) ACBUC *acbuc;

/// Config

+ (ACBUC *) getACBUC;

@end

